final: self: let
  inherit (self) rustPlatform fetchFromGitea lib;
in {
  sash = rustPlatform.buildRustPackage rec {
    pname = "sash";
    version = "0.1.0";

    src = fetchFromGitea {
      domain = "codeberg.org";
      owner = "felipeqq2";
      repo = pname;
      rev = "v${version}";
      sha256 = "sha256-2bhLcNHslcjqytd11/ceMsjTivLa91FrIcHIURErDhI=";
    };

    cargoHash = "sha256-Zgw6PIzKsaZnL+E2Bia35H2e+NdzN5q4NKnURJqiQzU=";

    meta = with lib; {
      description = "A simple local dashboard";
      homepage = "https://codeberg.org/felipeqq2/sash";
      license = licenses.bsd0;
    };
  };
}
