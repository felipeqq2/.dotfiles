{nixpkgs, ...}: let
  files = with builtins;
    map (file: ./. + "/${file}")
    (filter (f: baseNameOf f != "default.nix") (attrNames (readDir ./.)));
in
  nixpkgs.lib.composeManyExtensions (map import files)
