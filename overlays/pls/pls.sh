#!/usr/bin/env bash

set -eo pipefail

PKGS_PREFIX=self#pkgs.@system@.nixpkgs.

VAULT_FILE="/srv/syncthing/001.003_Vault/001.003_Vault.org"
OUT_DIR="$HOME/300_Projects/300_Misc/301.004_Website/"
DB_FILE="/srv/syncthing/001.003_Vault/kartes.db"

case "$1" in
    kartes-sqlite)
        case "$2" in
            org)
                $0 run kartes.sqlite index-org "$VAULT_FILE" --db "$DB_FILE";;

            "") echo "No command specified"; exit 1;;
            *) echo "Unknown command"; exit 1;;
        esac;;
    kartes-web)
        $0 run kartes.web --db "$DB_FILE" --out "$OUT_DIR"

        if [ "$2" = "--commit" ]
        then
           CURRENT_PWD=$(pwd)
           cd "$OUT_DIR"
           git add -A
           git commit -m "$(date --iso-8601=seconds --utc): kartes update"
           git push
           cd "$CURRENT_PWD"
        fi;;
    run) nix run "$PKGS_PREFIX$2" -- "${@:3}";;
    sh) nix shell $(for pkg in "${@:2}"; do echo "$PKGS_PREFIX$pkg"; done);;
    env) nix develop "self#$2" -c "$SHELL";;

    "") echo "No command specified"; exit 1;;
    *) echo "Unknown command"; exit 1;;
esac
