final: self: let
  inherit (self) stdenvNoCC installShellFiles makeWrapper lib;
in {
  pls = stdenvNoCC.mkDerivation {
    name = "pls";

    nativeBuildInputs = [installShellFiles makeWrapper];
    src = ./.;

    # substitute @system@
    patchPhase = ''
      substituteAllInPlace pls.sh
    '';

    installPhase = ''
      install -D pls.sh $out/bin/pls
      installShellCompletion completions/pls.*
    '';

    postFixup = ''
      wrapProgram "$out/bin/pls" --prefix PATH : \
      "${with final;
        lib.makeBinPath [
          toybox

          # for kartes
          git
          gnupg
        ]}"
    '';

    meta = with lib; {
      description = "pls, do this easy repetitive task";
      platforms = platforms.all;
    };
  };
}
