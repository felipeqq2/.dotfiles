function __first_param
    return (test (count (commandline -poc)) -lt 2)
end

# no files
complete -c pls -f

complete -c pls -n __first_param -a kartes-sqlite -d 'Index Kartes data' -r
complete -c pls -n "__fish_seen_subcommand_from kartes-sqlite" -a "org" -d 'Use org file'

complete -c pls -n __first_param -a kartes-web -d 'Build Kartes web page' -r
complete -c pls -n "__fish_seen_subcommand_from kartes-web" -a "--commit" -d 'Commit updates to repos'

complete -c pls -n __first_param -a run -d 'Run nixpkgs application' -r
complete -c pls -n "__fish_seen_subcommand_from run" -F

complete -c pls -n __first_param -a sh -d 'Create new shell with nixpkgs applications' -r
complete -c pls -n __first_param -a env -d 'Create new shell with selected dev environment' -r
