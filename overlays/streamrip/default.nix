final: self: let
  inherit (final) python3Packages poetry2nix fetchFromGitHub lib;
in {
  streamrip = poetry2nix.mkPoetryApplication rec {
    pname = "streamrip";
    version = "2.0.5";

    projectDir = fetchFromGitHub {
      owner = "nathom";
      repo = pname;
      rev = "v${version}";
      hash = "sha256-KwMt89lOPGt6nX7ywliG/iAJ1WnG0CRPwhAVlPR85q0=";
    };

    patches = [./path_assert.patch];

    # disable windows-curses
    overrides = poetry2nix.overrides.withDefaults (final: self: {
      # windows only dependencies
      windows-curses = null;
      pick = null;

      # patches for... whatever shit they broke the last update
      ruff = self.ruff.override {preferWheel = true;};
      inherit (python3Packages) pillow;
    });

    meta = with lib; {
      description = "A scriptable music downloader for Qobuz, Tidal, SoundCloud, and Deezer";
      homepage = "https://github.com/nathom/streamrip";
      license = licenses.gpl3Only;
      mainProgram = "rip";
    };
  };
}
