# Gaming on Linux
#
# Lutris is cool, but it really sucks when running Epic. Mostly because the Epic Launcher wasn't
# made for Linux (and it lowkey sucks, even on Windows). So I'm using Legendary for Epic. And not
# really using Lutris.
final: self: {
  # add missing dependencies to lutris
  lutris = self.lutris.override {
    extraPkgs = pkgs: with pkgs; [dwarfs fuse-overlayfs bubblewrap wget wineWowPackages.waylandFull];
  };

  wine = final.wineWowPackages.waylandFull;
}
