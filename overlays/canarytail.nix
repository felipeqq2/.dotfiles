final: self: let
  inherit (self) buildGoModule fetchFromGitHub lib;
in {
  canarytail = buildGoModule rec {
    pname = "canarytail";
    # latest version is not so latest
    version = "feb22a566f909d301d4ba72d1d13cfa28ffb22ff";

    src = fetchFromGitHub {
      owner = "canarytail";
      repo = "client";
      rev = "${version}";
      sha256 = "sha256-+v9dqhkDYlcVJMQ1cEmQ0opvYxvtyE9Aet70F4G+kvA=";
    };

    subPackages = ["cmd"];

    vendorSha256 = "sha256-y38TZXmlAPW+tABbakQntbCvU8TeuTYgxmgAcwnMoD0=";

    installPhase = ''
      runHook preInstall

      install -D -m 555 -T $GOPATH/bin/cmd $out/bin/canarytail

      runHook postInstall
    '';

    meta = with lib; {
      homepage = "https://canarytail.org";
      license = licenses.gpl3Only;
      platform = platforms.all;
    };
  };
}
