final: self: let
  inherit (self) buildGoModule fetchFromGitea lib;
in {
  webmun = buildGoModule rec {
    pname = "webmun";
    version = "0.2.1";

    src = fetchFromGitea {
      domain = "codeberg.org";
      owner = "felipeqq2";
      repo = pname;
      rev = "v${version}";
      hash = "sha256-fMIlunH2zpQ+HPY5RSLnBXYiNp08+9gS42JX3jkJmBE=";
    };

    vendorHash = "sha256-3j2Wjz1FMmRheiXtcQyfuNNgjeDak4r+brCAgKWaAOQ=";

    ldflags = ["-X codeberg.org/felipeqq2/webmun/helpers.version=v${version}"];

    meta = with lib; {
      license = licenses.ecl20;
      platform = platforms.all;
    };
  };
}
