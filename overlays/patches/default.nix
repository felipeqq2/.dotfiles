(final: prev: {
  # yeah, I'm using a tiling WM (Sway!). It's broken without this because Java.
  android-studio = prev.android-studio.override {tiling_wm = true;};

  # adblock spotify
  spotify = prev.nur.repos.nltch.spotify-adblock.spotify-adblocked;

  # i know XOAUTH2 is obsolete and this relies on deprecated packages but well, fuck google
  isync = prev.isync.override {withCyrusSaslXoauth2 = true;};

  soulseekqtWithDir = dir:
    prev.soulseekqt.overrideAttrs (oldAttrs: {
      postFixup = ''wrapProgram "$out/bin/SoulseekQt" --set HOME ${dir}/soulseekqt'';
    });

  prismlauncher-unwrapped =
    prev.prismlauncher-unwrapped.overrideAttrs
    (old: {
      patches = (old.patches or []) ++ [./prismlauncher.patch];
    });
})
