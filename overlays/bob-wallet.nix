final: self: let
  inherit (self) appimageTools fetchurl lib;
in {
  bob-wallet = appimageTools.wrapType2 rec {
    pname = "bob-wallet";
    version = "2.1.0";

    src = fetchurl {
      url = "https://github.com/kyokan/bob-wallet/releases/download/v${version}/Bob-${version}.AppImage";
      hash = "sha256-fce+A4rba792OiGXOfuIoNG+Q7R6KG5J2Yy4Rp+OhlM=";
    };

    meta = with lib; {
      description = "GUI for DNS Record Management and Name Auctions on Handshake";
      homepage = "https://bobwallet.io";
      licence = licenses.gplv3Only;
      platforms = ["x86_64-linux"];
    };
  };
}
