{
  pkgs,
  config,
  lib,
  inputs,
  ...
}: {
  my = {
    metal = true;
    gui = {
      enable = true;
      wallpaper = pkgs.fetchurl {
        url = "https://w.wallhaven.cc/full/4o/wallhaven-4oj17p.jpg";
        hash = "sha256-OfBQOE8r8mk4e5Vhvw75LwDTHS8lAm5jNBqMbI0g1vw=";
      };
    };
    email = true;
    media = true;
    backup.enableBackblaze = true;
    sash.enable = true;
  };

  time.timeZone = "America/Sao_Paulo";

  services = {
    tailscale.enable = true;
    fstrim.enable = true; # trim ssd regularly
  };

  services.syncthing = {
    enable = true;
    settings.folders = {
      "/srv/syncthing/010.012_Pictures" = {
        id = "pictures";
        label = "Photos";
        devices = ["A53"];
      };
      "/srv/syncthing/keys" = {
        id = "keys";
        label = "Keys";
        devices = ["A53"];
      };
      "/srv/syncthing/001.003_Vault" = {
        id = "vault";
        label = "Vault";
        devices = ["A53"];
      };
    };
  };

  services.restic.backups.backblaze.paths = [
    "/srv/syncthing"
    "/srv/010.002_Ebooks"
    "/mnt/media/010.006_Music"
    "/home/${config.my.username}/010_Resources"
    "/home/${config.my.username}/300_Projects"
    "/home/${config.my.username}/${config.my.xdg.data}/email"
  ];

  # keyboard layout
  services.xserver.xkb = {
    layout = "br,carpalx";
    variant = ",qgmlwy";
    model = "acer_laptop";
    options = "grp:win_space_toggle,compose:menu";

    extraLayouts.carpalx = {
      description = "Highly optimized keyboard layout.";
      languages = ["eng"];
      symbolsFile = pkgs.fetchurl {
        url = "https://www.khjk.org/log/2011/jan/carpalx.xkb";
        sha256 = "sha256-e7M7Enc8qoOoTVqvIDH0rm8ST8yDtoD0Z6qOMNolJtk=";
      };
    };
  };

  home-manager.users.${config.my.username} = {
    home.packages = [(pkgs.soulseekqtWithDir "/home/${config.my.username}/${config.my.xdg.config}")];

    colorScheme = inputs.nix-colors.colorSchemes.gruvbox-dark-hard;
    wayland.windowManager.sway = {
      # using an external keyboard layout breaks home-manager checks
      checkConfig = false;

      # move windows out of main display if lid is off (used for docking)
      extraConfig = ''
        bindswitch --reload --locked lid:on output eDP-1 disable
        bindswitch --reload --locked lid:off output eDP-1 enable
      '';
    };

    programs.beets = {
      enable = true;
      settings = {
        directory = "/mnt/media/010.006_Music/Archive";
        library = "/mnt/media/010.006_Music/beets.db";
      };
    };
  };

  swapDevices = [{device = "/swapfile";}];

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi = {
        canTouchEfiVariables = true;
        efiSysMountPoint = "/boot";
      };
    };

    supportedFilesystems = ["ntfs" "btrfs"];

    initrd = {
      secrets = {"/crypto_keyfile.bin" = null;};
      availableKernelModules = ["nvme" "xhci_pci" "ahci" "usb_storage" "usbhid" "sd_mod"];
      kernelModules = ["kvm-amd" "amdgpu"];

      luks.devices."luksroot" = {
        device = "/dev/disk/by-uuid/6d05db78-c54e-459b-9250-4d3577246f82";
        bypassWorkqueues = true; # improve performance on SSDs
        allowDiscards = true; # enable fstrim support
      };
    };
  };

  fileSystems = {
    "/boot" = {
      device = "/dev/disk/by-uuid/C4DC-C58B";
      fsType = "vfat";
      options = ["fmask=0077" "dmask=0077"];
    };
    "/" = {
      device = "/dev/mapper/luksroot";
      fsType = "ext4";
    };
    "/mnt/media" = {
      device = "/dev/disk/by-label/media";
      options = ["nofail" "x-systemd.device-timeout=5s"];
    };
  };

  # allow transmission mount access
  systemd.services.transmission = {
    serviceConfig.BindPaths = ["/mnt"];
    after = ["mnt-media.mount"];
  };

  networking.useDHCP = lib.mkDefault true;

  hardware = {
    enableRedistributableFirmware = true;
    cpu.amd.updateMicrocode = true;
    graphics = {
      enable = true;

      enable32Bit = true;

      extraPackages = with pkgs; [
        vaapiVdpau
        libvdpau-va-gl
        rocmPackages.clr
        amdvlk
      ];
    };
  };

  environment.variables.AMD_VULKAN_ICD = lib.mkDefault "RADV";
}
