# SSH/Age/PGP public keys
{
  felipeqq2 = {
    pgp = "7391BF2DA2C3B2C9BE25ACA9C7A74616F3025DF4";
    age = [
      "age1qm65gel6lgz2raqlq0s8e6dncd0ekx5v4dwvfctc5qd0hpxwvyrqw66v83" # Recovery key
      "age1yubikey1qwhyqcuy8nce0yhrv9a2wxj76eqxs7h00jmmv4k3h97waysxxnvj5k6plh4" # Yubikey 22510564
      "age1yubikey1qvldsqzs0jwqpm9a93crhvtnkwtx9qjpuvpde2h06pjrqrdh42agxaqjhuh" # Yubikey 22510871
    ];

    # derived from PGP key
    ssh = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO1LoA/0cYULkAXcPwevGaR+k69DkS2DvUtB8XnGkU3q felipeqq2"];
  };

  sigma = ["ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIFKsBvq3DV7snnurlDL+JS6GSxPT7xltel6QiJlBMV6h root@sigma"];
}
