with import ../keys.nix; {
  "npmrc.age".publicKeys = felipeqq2.age ++ sigma;
  "wakatime.cfg.age".publicKeys = felipeqq2.age ++ sigma;
  "beets.yaml.age".publicKeys = felipeqq2.age ++ sigma;
  "password.age".publicKeys = felipeqq2.age ++ sigma;
  "email-password.age".publicKeys = felipeqq2.age ++ sigma;
  "conpec-email-password.age".publicKeys = felipeqq2.age ++ sigma;
  "rust-credentials.toml.age".publicKeys = felipeqq2.age ++ sigma;
  "google-oauth.json.age".publicKeys = felipeqq2.age ++ sigma;
  "wireless.env.age".publicKeys = felipeqq2.age ++ sigma;
  "hut.age".publicKeys = felipeqq2.age ++ sigma;
  "vercel-conpec-auth.json.age".publicKeys = felipeqq2.age ++ sigma;
  "backup-backblaze.env.age".publicKeys = felipeqq2.age ++ sigma;
  "backup-rclone.conf.age".publicKeys = felipeqq2.age ++ sigma;
  "listenbrainz-token.age".publicKeys = felipeqq2.age ++ sigma;
}
