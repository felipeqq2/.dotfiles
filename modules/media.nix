# Common architecture around Jellyfin/-arr apps and Transmission torrent client.
#
# All services are part of the `media` group to be able to pass the files around. Note that most of
# these services can't be (easily) configured by Nix, so they each have a lot of specific
# configuration handled by their respective interfaces.
{
  lib,
  config,
  pkgs,
  ...
}: {
  options.my.media = lib.mkEnableOption "Enable media server";

  config = lib.mkIf config.my.media {
    users.groups.media.members = [config.my.username];

    services = {
      jellyfin = {
        enable = true;
        openFirewall = true;
        group = "media";
      };
      transmission = {
        enable = true;
        group = "media";
        openFirewall = true;
        package = pkgs.transmission_4;
        settings = {
          incomplete-dir-enabled = false;
          download-dir = "/srv/torrents";
        };
      };
    };

    # related files/directories
    systemd.tmpfiles.rules = [
      # 2xxx mode: 2 = SGID (all files created in directory will have same group as parent)
      "d /srv/torrents 2770 - media"
    ];

    my.sash.links = [
      {
        name = "Transmission";
        url = "http://localhost:9091";
        icon = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/transmission.svg";
      }
      {
        name = "Jellyfin";
        url = "http://localhost:8096";
        icon = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/jellyfin.svg";
      }
    ];
  };
}
