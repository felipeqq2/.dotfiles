# DNS is managed by systemd-resolved.
#
# Mullvad (with ad-blocking) is enabled by default. Quad9 is used as fallback.
_: {
  services.resolved = {
    enable = true;
    dnssec = "true";
    dnsovertls = "true";
    fallbackDns = [
      # fallback to default quad9 setup (malware blocking + DNSSEC)
      "2620:fe::fe#dns.quad9.net"
      "2620:fe::9#dns.quad9.net"
      "9.9.9.9#dns.quad9.net"
      "149.112.112.112#dns.quad9.net"
    ];
  };

  networking.nameservers = [
    "2a07:e340::4#base.dns.mullvad.net"
    "194.242.2.4#base.dns.mullvad.net"
  ];
}
