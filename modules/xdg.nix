# Configure and setup XDG directories (see apps/xdg.nix).
{
  config,
  lib,
  ...
}: {
  options.my.xdg = with lib; let
    strDefault = default:
      mkOption {
        type = types.str;
        inherit default;
      };
  in {
    cache = strDefault ".xdg/cache";
    config = strDefault ".xdg/config";
    data = strDefault ".xdg/data";
    state = strDefault ".xdg/state";
  };

  # Set up environment variables early (by PAM).
  # The default shell gets access to them right away
  # (and is then able to load its own configuration files).
  config.environment.sessionVariables = let
    my = config.my.xdg;
  in {
    XDG_CACHE_HOME = "$HOME/${my.cache}";
    XDG_CONFIG_HOME = "$HOME/${my.config}";
    XDG_DATA_HOME = "$HOME/${my.data}";
    XDG_STATE_HOME = "$HOME/${my.state}";
  };
}
