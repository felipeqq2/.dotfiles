args @ {
  lib,
  config,
  pkgs,
  inputs,
  ...
}: {
  # import everything inside this folder
  imports = with builtins;
    map (file: ./. + "/${file}")
    (filter (f: baseNameOf f != "default.nix") (attrNames (readDir ./.)));

  options.my = with lib; {
    username = mkOption {
      type = types.str;
      default = "felipeqq2";
      description = "Default username";
    };

    battery = mkOption {
      type = types.bool;
      default = false;
      description = "Host has battery.";
    };

    email = mkEnableOption "Email service";
  };

  config = {
    system.stateVersion = "22.05";

    boot = {
      initrd.systemd.enable = true;
      loader = {
        timeout = null;
        systemd-boot = {
          editor = false;
          configurationLimit = 20;
        };
      };

      kernel.sysctl."net.core.rmem_max" = lib.mkForce 5000000;
      tmp = {
        useTmpfs = true;
        tmpfsSize = "90%";
      };
    };

    # use /var/tmp, the tmp directory for large files, when building with nix
    systemd.services.nix-daemon = {
      environment.TMPDIR = "/var/tmp";
    };

    users = {
      mutableUsers = false;
      users.${config.my.username} = {
        isNormalUser = true;
        extraGroups = ["wheel"];
        description = "Felipe Silva";
        hashedPasswordFile = config.age.secrets.password.path;

        openssh.authorizedKeys.keys = (import ../keys.nix).felipeqq2.ssh;

        shell = pkgs.fish;
      };
    };

    environment.defaultPackages = lib.mkForce []; # remove default packages (perl, nano and friends)

    security.sudo.execWheelOnly = true;

    programs = {
      fish.enable = true;
      command-not-found.enable = false;
      dconf.enable = true;
      fuse.userAllowOther = true;
    };

    # udev 250 doesn't reliably reinitialize devices after restart
    # (see GitHub issue nixos/nixpkgs#180175)
    systemd.services.systemd-udevd.restartIfChanged = false;

    services = {
      journald.extraConfig = ''
        Storage=volatile
      '';

      udisks2.enable = true; # make calibre work
      dbus.implementation = "broker"; # faster dbus
      irqbalance.enable = true; # balance cpu load
    };

    networking = {
      nftables.enable = true;
      dhcpcd.extraConfig = "nohook resolv.conf";
      firewall.trustedInterfaces = ["tailscale0"]; # trust tailscale
    };

    i18n = {
      defaultLocale = "en_GB.utf8";
      extraLocaleSettings = {
        LC_ADDRESS = "pt_BR.utf8";
        LC_IDENTIFICATION = "pt_BR.utf8";
        LC_MEASUREMENT = "pt_BR.utf8";
        LC_MONETARY = "pt_BR.utf8";
        LC_NAME = "pt_BR.utf8";
        LC_NUMERIC = "pt_BR.utf8";
        LC_PAPER = "pt_BR.utf8";
        LC_TELEPHONE = "pt_BR.utf8";
        LC_TIME = "en_GB.utf8"; # i want dates to be formatted in English.
      };
    };

    nix = {
      # flake-utils-plus specific config
      generateRegistryFromInputs = true;
      generateNixPathFromInputs = true;
      linkInputs = true;

      # Enable flakes
      package = pkgs.nixVersions.latest;
      settings.experimental-features = "nix-command flakes";

      gc = {
        automatic = lib.mkIf (!(config.programs.nh.enable && config.programs.nh.clean.enable)) true;
        dates = "weekly";
        options = "--delete-older-than 30d";
      };

      settings = {
        auto-optimise-store = true;
        use-xdg-base-directories = true;
        warn-dirty = false; # stop yelling about a dirty git tree
        trusted-users = ["root" "@wheel"]; # trust wheel group with additional rights

        # disable cache
        # substitute = false;
        substituters = ["https://cache.nixos.org" "https://nix-community.cachix.org"];
        trusted-public-keys = [
          "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
        ];
      };
    };

    # Home manager config
    home-manager = {
      backupFileExtension = ".bkp";
      useGlobalPkgs = true;
      useUserPackages = true;
      extraSpecialArgs = {inherit inputs;};
      users.${config.my.username} = import ../home.nix;
    };
  };
}
