# Restic-based backup to cloud storage (namely, Backblaze B2 and Google Drive).
#
# Hosts should specify `services.restic.backups.${backup}.paths`.
{
  lib,
  pkgs,
  config,
  ...
}: {
  options.my.backup = with lib; {
    enableBackblaze = mkEnableOption "Backblaze backup";
  };

  config = {
    services.restic.backups = {
      backblaze = lib.mkIf config.my.backup.enableBackblaze {
        runCheck = true;
        exclude = ["node_modules" "target" ".direnv"];
        environmentFile = config.age.secrets.backup-backblaze.path;

        timerConfig = {
          OnCalendar = "daily";
          RandomizedDelaySec = "2h";
          Persistent = true;
        };
      };

      google-to-backblaze = {
        paths = ["/mnt/gdrive"];

        runCheck = true;
        createWrapper = false;
        environmentFile = config.age.secrets.backup-backblaze.path;
        timerConfig = null;
      };
    };

    environment.systemPackages = [pkgs.rclone];
    systemd.services.restic-backups-google-to-backblaze = {
      wants = ["mnt-gdrive.mount"];
      after = ["mnt-gdrive.mount"];
    };

    systemd.mounts = [
      {
        requiredBy = ["restic-backus-google-to-backblaze.service"];
        before = ["restic-backus-google-to-backblaze.service"];

        wants = ["network-online.target"];
        after = ["network-online.target"];
        where = "/mnt/gdrive";
        what = "google:";

        description = "Rclone Google Drive mount";
        type = "rclone";
        options = "read-only,umask=277,config=${config.age.secrets.backup-rcloneconf.path}";
      }
    ];
  };
}
