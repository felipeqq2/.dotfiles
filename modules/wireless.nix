# Static wireless networking with wpa_supplicant.
{
  config,
  pkgs,
  ...
}: {
  environment.systemPackages = [pkgs.wpa_supplicant_gui];

  networking.wireless = {
    enable = true;
    secretsFile = config.age.secrets.wireless-env.path;
    userControlled.enable = true;
    networks = {
      "Felipe's Galaxy A53 5G".pskRaw = "ext:PASS_GALAXY";
      "Kit Sergio 17".pskRaw = "ext:PASS_KITSERGIO";
      "AP_1503_5G".pskRaw = "ext:PASS_1503";
      "ALHN-E09E-11ac".pskRaw = "ext:PASS_AP201";
      eduroam = {
        authProtocols = ["WPA-EAP"];
        auth = let
          cert = pkgs.fetchurl {
            url = "https://catalogo.detic.unicamp.br/detic/sites/default/files/tutoriais/ca.pem";
            hash = "sha256-pU3NtWAyu8XyqrjMN3k0BOlGluhUvDqIj/QUTzyEeFM=";
          };
        in ''
          pairwise=CCMP
          group=CCMP TKIP
          eap=TTLS
          phase2="auth=PAP"
          altsubject_match="DNS:radius1.unicamp.br;DNS:radius2.unicamp.br"
          ca_cert="${cert}"
          identity="204706@unicamp.br"
          password=ext:PASS_DACUNICAMP
        '';
      };
    };
  };
}
