{
  lib,
  pkgs,
  config,
  inputs,
  ...
}: {
  options.my = with lib; {
    gui = {
      enable = mkEnableOption "Graphical User Interface";

      wallpaper = mkOption {
        type = types.path;
        description = "Desktop wallpaper";
        default = pkgs.fetchurl {
          url = "https://raw.githubusercontent.com/NixOS/nixos-artwork/master/wallpapers/nix-wallpaper-simple-dark-gray.png";
          sha256 = "sha256-JaLHdBxwrphKVherDVe5fgh+3zqUtpcwuNbjwrBlAok=";
        };
      };

      blurWallpaper = mkOption {
        type = types.bool;
        description = "Whether to blur wallpaper on lock screen";
        default = false;
      };

      google-fonts = mkEnableOption "Google Fonts";
    };
  };

  config = lib.mkIf config.my.gui.enable {
    programs.sway = {
      enable = true;
      extraPackages = lib.mkForce []; # I wanna keep control of my machine!
      wrapperFeatures.gtk = true; # make gtk happy!
    };

    # login as me automatically (ideal for single-user machines)
    services.getty.autologinUser = config.my.username;

    # Enable XDG portal (needed for... multiple things, i.e. Wayland screen sharing)
    xdg.portal = {
      enable = true;
      wlr.enable = true; # wlroots support
      # needed for certain GTK features, like libadwaita dark mode support
      extraPortals = [pkgs.xdg-desktop-portal-gtk];
    };

    home-manager.users.${config.my.username}.colorScheme =
      lib.mkDefault inputs.nix-colors.colorSchemes.twilight;

    # MESA cache shouldn't clutter $HOME
    #
    # some applications can't access /var/cache/mesa for some reason (like Lutris). Honestly, fuck
    # it. I already spent too much time fiddling with this.
    environment.variables = {
      MESA_SHADER_CACHE_DIR = "/var/cache/mesa";
      MESA_GLSL_CACHE_DIR = "/var/cache/mesa";
    };
    systemd.globalEnvironment = {
      MESA_SHADER_CACHE_DIR = "/var/cache/mesa";
      MESA_GLSL_CACHE_DIR = "/var/cache/mesa";
    };
    systemd.tmpfiles.rules = ["d /var/cache/mesa 1777 root root 10d"];

    fonts = {
      packages = with pkgs;
        [
          # fonts not available with GF
          dejavu_fonts
          font-awesome
          joypixels
          emacs-all-the-icons-fonts
          times-newer-roman
        ]
        ++ (
          if config.my.gui.google-fonts
          then [google-fonts]
          else [
            # fonts available with GF
            jetbrains-mono
            overpass
            source-serif
          ]
        );
      fontDir.enable = true;
      fontconfig.defaultFonts = {
        monospace = ["Jetbrains Mono"];
        sansSerif = ["Overpass"];
        serif = ["DejaVu Serif"];
        emoji = ["Joypixels"];
      };
    };

    # fix https://wiki.archlinux.org/title/Wayland#Java
    environment.variables._JAVA_AWT_WM_NONREPARENTING = "1";
  };
}
