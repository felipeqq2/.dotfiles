{
  lib,
  pkgs,
  config,
  ...
}: {
  options.my.keys.yubikey = lib.mkOption {
    type = lib.types.bool;
    default = true;
    description = "Enable YubiKey support.";
  };

  config = lib.mkIf (config.my.keys.yubikey && config.my.metal) {
    # see https://nixos.wiki/wiki/Yubikey#pam_u2f for setup
    security.pam.u2f = {
      enable = true;
      settings = {
        cue = true;
        authfile = ./u2f_keys;
      };
    };

    services.pcscd.enable = true;

    environment.systemPackages = with pkgs; [
      yubikey-manager
      yubioath-flutter
      age-plugin-yubikey
    ];

    systemd.user.services.yubikey-touch-detector = {
      description = "YubiKey touch detector";

      serviceConfig = {
        ExecStart = "${pkgs.yubikey-touch-detector}/bin/yubikey-touch-detector --libnotify";
        Environment = ["PATH=${lib.makeBinPath [pkgs.gnupg]}"];
      };

      wantedBy = ["graphical-session.target"];
    };
  };
}
