{config, ...}: {
  age.secrets = let
    withUser = file: {
      inherit file;
      mode = "400";
      owner = config.my.username;
      group = "users";
    };
  in {
    password.file = ../secrets/password.age;
    wireless-env.file = ../secrets/wireless.env.age;
    backup-backblaze.file = ../secrets/backup-backblaze.env.age;
    backup-rcloneconf.file = ../secrets/backup-rclone.conf.age;

    npmrc = withUser ../secrets/npmrc.age;
    wakatime = withUser ../secrets/wakatime.cfg.age;
    beets = withUser ../secrets/beets.yaml.age;
    hut = withUser ../secrets/hut.age;
    email-password = withUser ../secrets/email-password.age;
    conpec-email-password = withUser ../secrets/conpec-email-password.age;
    rust-credentials = withUser ../secrets/rust-credentials.toml.age;
    google-oauth = withUser ../secrets/google-oauth.json.age;
    vercel-conpec-auth = withUser ../secrets/vercel-conpec-auth.json.age;
    listenbrainz-token = withUser ../secrets/listenbrainz-token.age;
  };
}
