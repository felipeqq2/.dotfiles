{
  lib,
  config,
  pkgs,
  ...
}: {
  options.my.sash = with lib; {
    enable = mkEnableOption "sash";

    port = mkOption {
      type = types.int;
      default = 9000;
    };

    links = let
      linkSub = types.submodule {
        options = {
          url = mkOption {type = types.str;};
          name = mkOption {type = types.str;};
          icon = mkOption {
            type = types.nullOr types.str;
            default = null;
          };
        };
      };
    in
      mkOption {
        type = with types; listOf linkSub;
        default = [];
      };
  };

  config = lib.mkIf config.my.sash.enable {
    home-manager.users.${config.my.username} = {
      systemd.user.services.sash = {
        Service = {
          ExecStart = "${pkgs.sash}/bin/sash";
          ExecReload = "${pkgs.coreutils}/bin/kill -SIGUSR2 $MAINPID";
          Restart = "on-failure";
        };
        Install.WantedBy = ["default.target"];
      };

      xdg.configFile."sash/config.json" = {
        text = builtins.toJSON config.my.sash;
        onChange = "${pkgs.procps}/bin/pkill -u $USER -USR2 sash || true";
      };
    };
  };
}
