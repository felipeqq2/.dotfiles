{
  config,
  pkgs,
  lib,
  ...
}: {
  virtualisation.docker.enableOnBoot = false;
  users.users.${config.my.username}.extraGroups = ["docker"];
}
