# This service runs as its own user. So, to be able to access the files, each synced folder should
# be made available in `/srv/syncthing`.
#
# Host-specific configuration (like which folders should be synced) are declared separately.
{
  lib,
  config,
  ...
}: let
  cfg = config.services.syncthing;
in
  lib.mkIf cfg.enable {
    services.syncthing = {
      settings.devices = {
        A53 = {
          id = "7KEPKHM-SWUU4LL-MND62PY-C3D4AQZ-42RSA6X-IIUICOK-PUUG5VZ-ZSUXFQI";
        };

        sigma = {
          id = "JIW76CL-OTYLOE2-DSJWJWT-FPWFBUY-M4TNJCK-7XFW56I-Z3ZMHR2-WMTGBQD";
        };
      };

      openDefaultPorts = true;
    };

    users.users.${config.my.username}.extraGroups = [cfg.group];

    systemd.tmpfiles.rules = [
      # 2xxx mode: 2 = SGID (all files created in directory will have same group as parent)
      "d /srv/syncthing 2770 ${cfg.user} ${cfg.group}"
      "A /srv/syncthing - - - - g:${cfg.group}:rwx"
    ];

    my.sash.links = [
      {
        name = "Syncthing";
        url = "http://localhost:8384";
        icon = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/syncthing.svg";
      }
    ];
  }
