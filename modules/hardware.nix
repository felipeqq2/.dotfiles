{
  pkgs,
  config,
  lib,
  ...
}: {
  options.my.metal = lib.mkOption {
    type = lib.types.bool;
    description = "Whether the system runs in bare metal.";
    default = false;
  };

  config = lib.mkIf config.my.metal {
    services = {
      printing = {
        enable = true;
        browsing = true;
        drivers = with pkgs; [epson-escpr];
      };

      # needed for Bonjour printing configuration (dnssd)
      avahi = {
        enable = true;
        # see https://github.com/apple/cups/issues/5357
        nssmdns4 = true;
      };
    };

    hardware.bluetooth.enable = true;

    # Enable sound with pipewire.
    hardware.pulseaudio.enable = false;
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    environment.systemPackages = [pkgs.pavucontrol];
  };
}
