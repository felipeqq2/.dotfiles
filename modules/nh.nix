{
  lib,
  config,
  ...
}: {
  programs.nh = {
    enable = lib.mkDefault true;
    flake = "/home/${config.my.username}/300_Projects/300_Misc/301.002_Dotfiles";
  };
}
