{
  config,
  pkgs,
  lib,
  ...
}: {
  my.sash.links = lib.mkIf config.services.kubo.enable [
    {
      name = "IPFS";
      url = "http://localhost:5001/webui";
      icon = "https://cdn.jsdelivr.net/gh/walkxcode/dashboard-icons/svg/ipfs.svg";
    }
  ];

  services.kubo = {
    enableGC = true;
    autoMount = false;
    settings = {
      Mounts = {
        IPNS = "/mnt/ipns";
        IPFS = "/mnt/ipfs";
      };
      Addresses.API = "/ip4/127.0.0.1/tcp/5001";
    };
  };

  users.users.${config.my.username}.extraGroups = [config.services.kubo.group];
}
