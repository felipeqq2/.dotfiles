pkgs:
with pkgs; let
  shell = p: mkShell {buildInputs = p;};
  nodeBase = with nodePackages; [
    nodejs
    typescript-language-server
    typescript
  ];
in {
  # default devshell for dotfiles repo
  default = shell [ragenix nil statix lefthook];

  go = shell [go gopls gotools go-tools golangci-lint gox];

  haskell = shell [ghc haskellPackages.hoogle haskell-language-server];

  node = shell nodeBase;

  python = shell [python3 python3Packages.python-lsp-server];

  rust = shell [clang cmake rustc cargo rustfmt clippy rust-analyzer];

  web = with nodePackages; shell [vscode-langservers-extracted prettier];

  svelte = shell ([nodePackages.svelte-language-server] ++ nodeBase);

  deno = shell [deno];

  gcc = shell [gcc gnumake python3 valgrind gdb];

  maven = shell [jdt-language-server jdk maven];
}
