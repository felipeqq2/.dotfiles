# Personal .dotfiles

This is my repo for everything **NixOS**-related. It's literally my whole computer configuration (God bless NixOS), and a very personal one. I used another Git repo previously, but the log was a complete mess and I couldn't stand looking at it. So fuck these commits, I hope I'm in a solid state with no loose parts.

## Architecture
- `flake.nix`: starting point for everything.
- `environments.nix`: devshell for the dotfiles repo, and templates for non-nix projects.
- `keys.nix`: my age/ssh/pgp public keys.
- `secrets/`: self-descriptive, managed by [ragenix](https://github.com/yaxitech/ragenix).
- `overlays/`: my custom packages, or still not merged by upstream.
  - `patches/`: patches of already existing packages.
  - `pls/`: my custom shell tool to interact with Nix (and other things) — **pls**! (maybe I should just make custom fish functions/aliases)
- `modules/`: my global(-ish) NixOS configuration + options.
  - `default.nix`: everything to be applied globally.
  - `{module}.nix`: configuration for something specific (even if not enabled by default).
- `hosts/`: configuration for each of my machines.
- `home.nix`: global home-manager configuration.
- `apps/`: configuration for home-manager programs and services.

## Setup system

```bash
# Copy the keys (especially age, needed to decrypt the secrets)
cp -r /mnt/recover/my-keys ~/.keys

# Clone the repo
cd ~
git clone git@codeberg.org:felipeqq2/.dotfiles.git
cd .dotfiles

# Build the system
sudo nixos-rebuild switch --flake ".#$SYSTEM"

# Enable lefthook
lefthook install
```

## Caveats and workarounds

- Wayland can't launch graphical applications as root. Thus, any GUIs that need to be root, like gparted, should be launched with `sudo -E $PROGRAM`, keeping the `XDG_RUNTIME_DIR` environment variable. [See more](https://github.com/swaywm/sway/issues/4492).
