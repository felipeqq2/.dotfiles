{
  description = "My system-wide configuration";

  inputs = {
    nixpkgs.url = "github:felipeqq2/nixpkgs/fix/restic-cp";
    utilsplus.url = "github:gytis-ivaskevicius/flake-utils-plus";
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nur = {
      url = "github:nix-community/NUR";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    ragenix = {
      url = "github:yaxitech/ragenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nix-colors.url = "github:misterio77/nix-colors";
    kartes = {
      url = "git+https://codeberg.org/felipeqq2/kartes";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    orgtoical = {
      url = "git+https://codeberg.org/felipeqq2/orgtoical";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs: let
    inherit (inputs.nixpkgs) lib;
  in
    inputs.utilsplus.lib.mkFlake {
      inherit inputs;
      inherit (inputs) self;

      sharedOverlays = with inputs; [
        (import ./overlays inputs)
        emacs-overlay.overlay
        nur.overlays.default
        poetry2nix.overlays.default
        ragenix.overlays.default
        kartes.overlays.default
        orgtoical.overlays.default
      ];

      channelsConfig = {
        allowUnfree = true;
        joypixels.acceptLicense = true;
      };

      # NixOS configuration
      hostDefaults.modules = with inputs; [
        ./modules
        home-manager.nixosModules.home-manager
        ragenix.nixosModules.default
      ];

      # get all hosts (at ./hosts/{host}.nix)
      hosts = with builtins;
        lib.fold lib.recursiveUpdate {} (
          map (host: {
            ${lib.removeSuffix ".nix" host} = {
              modules = [(./hosts + "/${host}")];
              specialArgs = {inherit inputs;};
            };
          }) (attrNames (readDir ./hosts))
        );

      outputsBuilder = c: {
        devShells = import ./environments.nix c.nixpkgs;

        formatter = c.nixpkgs.alejandra;
      };
    };
}
