{
  config,
  osConfig,
  ...
}: {
  xdg.dataFile."com.vercel.cli/auth.json".source =
    config.lib.file.mkOutOfStoreSymlink osConfig.age.secrets.vercel-conpec-auth.path;

  # note: essa credencial é específica da Conpec. Deixo nos .dotfiles para não precisar ficar
  # logando toda hora que for usar. Mas isso significa que, para usar uma outra conta (como a conta
  # pessoal), preciso apagar esse arquivo antes. Ele vai ser linkado novamente durante o reboot, mas
  # enfim.
}
