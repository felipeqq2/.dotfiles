# XDG Desktop entries for not-installed, but commonly used apps.
#
# Each application should run with `nix-shell` (via `pls`). See specification[1] for allowed
# categories.
#
# [1]: https://specifications.freedesktop.org/menu-spec/menu-spec-latest.html#main-category-registry
{pkgs, ...}: let
  plsRun = str: "${pkgs.pls}/bin/pls run ${str}";
in {
  xdg.desktopEntries = {
    lutris = {
      name = "Lutris";
      genericName = "Game Launcher";
      exec = plsRun "lutris";
      categories = ["Game"];
      prefersNonDefaultGPU = true;
      icon = "lutris";
    };
    tor-browser = {
      name = "Tor Browser";
      genericName = "Private Onion Browser";
      exec = plsRun "tor-browser";
      categories = ["WebBrowser" "Network"];
      icon = "tor-browser";
    };
    libreoffice = {
      name = "Libreoffice";
      genericName = "Office Suite";
      exec = plsRun "libreoffice";
      categories = ["Office"];
      icon = "libreoffice";
      mimeType = [
        "application/msword" # .doc
        "application/vnd.openxmlformats-officedocument.wordprocessingml.document" # .docx
        "application/vnd.ms-excel" # .xls
        "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" # .xlsx
        "application/vnd.ms-powerpoint" # .ppt
        "application/vnd.openxmlformats-officedocument.presentationml.presentation" # .pptx
        "application/vnd.oasis.opendocument.chart" # .odc
        "application/vnd.oasis.opendocument.database" # .odb
        "application/vnd.oasis.opendocument.formula" # .odf
        "application/vnd.oasis.opendocument.graphics" # .odg
        "application/vnd.oasis.opendocument.presentation" # .odp
        "application/vnd.oasis.opendocument.spreadsheet" # .ods
        "application/vnd.oasis.opendocument.text" # .odt
      ];
    };
    digikam = {
      name = "Digikam";
      genericName = "Photo Management Software";
      exec = plsRun "digikam";
      categories = ["Graphics"];
      icon = "digikam";
    };
    vlc = {
      name = "VLC";
      genericName = "Media Player";
      exec = plsRun "vlc";
      categories = ["AudioVideo" "Player"];
      icon = "vlc";
      mimeType = [
        "application/ogg" # .ogx
        "audio/ogg" # .oga
        "video/ogg" # .ogv
        "audio/opus" # .opus
        "audio/flac" # .flac
        "video/mp4" # .mp4
        "video/mpeg" # .mpeg
        "audio/mpeg" # .mp3
        "video/webm" # .webm
        "audio/webm" # .weba
        "video/quicktime" # .mov
        "audio/aac" # .aac
        "video/x-matroska" # .mkv
      ];
    };
    josm = {
      name = "JOSM";
      genericName = "OpenStreetMap Editor";
      icon = "org.openstreetmap.josm";
      exec = plsRun "josm";
      categories = ["Education" "Maps" "Geoscience"];
    };
    calibre = {
      name = "Calibre";
      genericName = "E-book library management";
      icon = "calibre-gui";
      exec = plsRun "calibre";
      categories = ["Office"];
    };
    beeper = {
      name = "Beeper";
      genericName = "All in one messaging app";
      icon = "beeper";
      exec = plsRun "beeper --default-frame";
      categories = ["Network" "InstantMessaging"];
    };
  };
}
