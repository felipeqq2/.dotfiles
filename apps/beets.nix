# To enable, set programs.beets.enable to true, and configure:
# programs.beets.settings.directory (music files)
# programs.beets.settings.library (beets database)
{
  osConfig,
  config,
  pkgs,
  lib,
  ...
}: let
  beets = pkgs.beets.override (old: {
    pluginOverrides = {
      copyartifacts = {
        enable = true;
        propagatedBuildInputs = [pkgs.beetsPackages.copyartifacts];
      };
    };
  });
in
  lib.mkIf config.programs.beets.enable {
    programs = {
      beets = {
        package = beets;

        settings = {
          # include sensitive configuration: MusicBrainz and AcoustID credentials
          include = [osConfig.age.secrets.beets.path];

          asciify_paths = true;
          import = {
            move = true;
            languages = ["pt" "en"];
          };

          paths = {
            # Example: ABBA - Arrival [POLCD 272]/1-09. Tiger.flac
            default = "$albumartist - $album%aunique{} %if{$catalognum,[$catalognum]}/$disc-$track. $title";
            singleton = "Non-Album/$artist/$title";
            comp = "Compilations/$album%aunique{} %if{$catalognum,[$catalognum]}/$disc-$track. $title";
          };

          plugins = [
            "info"
            "scrub"
            "mbsync"
            "edit"
            "fetchart"
            "mbcollection"
            "replaygain"
            "chroma" # AcoustID
            "copyartifacts"
            "permissions"
          ];

          mbcollection = {
            auto = true;
            remove = true;
            collection = "93f15f63-20c5-4d90-9f2e-ae49ebb804c3";
          };
          replaygain.backend = "ffmpeg";
          fetchart.sources = [{coverart = "*";}]; # fetch from MusicBrainz Cover Art Archive
          copyartifacts.extensions = [".log" ".nfo" ".cue"];

          permissions = {
            # u=rw,g=rw,o=r
            file = 664;
            dir = 775;
          };
        };
      };
    };

    my.env.BEETSDIR = "${config.xdg.configHome}/beets";
  }
