_: {
  dconf.settings."org/gnome/Lollypop" = {
    import-playlists = true;
    import-advanced-artist-tags = true;
    artist-artwork = true;
    transitions = false;
    disable-scrobbling = false;
  };
}
