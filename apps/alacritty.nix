{
  config,
  lib,
  osConfig,
  pkgs,
  ...
}:
lib.mkIf osConfig.my.gui.enable {
  programs.alacritty = {
    enable = true;
    settings = {
      window = {
        padding = {
          x = 15;
          y = 15;
        };
        dynamic_title = false;
      };
      font.size = 10;

      # nix-colors theming (thanks https://github.com/aarowill/base16-alacritty)
      colors = with config.colorScheme.palette; {
        draw_bold_text_with_bright_colors = false;
        primary = {
          background = "0x${base00}";
          foreground = "0x${base05}";
        };
        cursor = {
          text = "0x${base00}";
          cursor = "0x${base05}";
        };
        normal = {
          black = "0x${base00}";
          red = "0x${base08}";
          green = "0x${base0B}";
          yellow = "0x${base0A}";
          blue = "0x${base0D}";
          magenta = "0x${base0E}";
          cyan = "0x${base0C}";
          white = "0x${base05}";
        };
        bright = {
          black = "0x${base03}";
          red = "0x${base09}";
          green = "0x${base01}";
          yellow = "0x${base02}";
          blue = "0x${base04}";
          magenta = "0x${base06}";
          cyan = "0x${base0F}";
          white = "0x${base07}";
        };
      };
    };
  };

  my.env.TERMINAL = "${pkgs.alacritty}/bin/alacritty";
}
