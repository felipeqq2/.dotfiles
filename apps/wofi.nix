{
  lib,
  pkgs,
  osConfig,
  ...
}:
lib.mkIf osConfig.my.gui.enable {
  programs.wofi = {
    enable = true;
    settings = {
      show = "drun";
      term = "${pkgs.alacritty}/bin/alacritty";
      allow_images = true;
      image_size = 24;
      gtk_dark = true; # wofi uses the GTK theme stylesheet (I think)
      no_actions = true; # disable multiple actions for apps like Firefox
    };
  };
}
