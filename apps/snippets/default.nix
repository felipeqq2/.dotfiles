{
  pkgs,
  osConfig,
  ...
}: {
  home.packages = [
    (pkgs.runCommand "snippets" {listenbrainzTokenFile = osConfig.age.secrets.listenbrainz-token.path;} ''
      mkdir -p $out/bin

      install -Dm644 ${./snippets.org} $out/bin/snippets.org
      substituteAllInPlace $out/bin/snippets.org

      ${pkgs.emacs-nox}/bin/emacs --batch --quick --load org $out/bin/snippets.org -f org-babel-tangle

      rm $out/bin/snippets.org
    '')
  ];
}
