#+TITLE: Code Snippets (shell functions, JS scripts, etc.)

* Music library
** Jellyfin: favorite all items in playlist
#+begin_src javascript
  const buttons = document.querySelectorAll('button[data-itemtype=Audio][data-isfavorite=false]')
  for (let el of buttons) {
      el.click()
  }
#+end_src
** Jellyfin: un-favorite all favorite albums
On the favorite screen:
#+begin_src javascript
  document.querySelectorAll('button[data-itemtype=Audio][data-isfavorite=true]')
  for (let el of buttons) {
      el.click()
  }
#+end_src
** Add liked track to playlist/Listenbrainz
#+begin_src bash :tangle addlikedtrack :shebang "#!/usr/bin/env nix-shell"
  #!nix-shell -i bash -p bash flac vorbis-tools curl toybox

  set -euo pipefail

  TOKEN=$(cat @listenbrainzTokenFile@)

  OLDDIR=$(pwd)
  cd "/mnt/media/010.006_Music/Archive"

  FILE=$(fzf)

  echo "../Archive/$FILE" >> ../Playlists/Liked.m3u8

  case $FILE in
      ,*.flac)
          RECTAG=$(metaflac --show-tag=MUSICBRAINZ_TRACKID "$FILE" | cut -c 21-);;
      ,*.ogg)
          RECTAG=$(vorbiscomment -l "$FILE" | grep MUSICBRAINZ_TRACKID | cut -c 21-);;
  esac

  curl -f -s -H "Authorization: Token $TOKEN" -H "Content-Type: application/json" \
       https://api.listenbrainz.org/1/feedback/recording-feedback \
       --data "{\"recording_mbid\": \"$RECTAG\", \"score\": 1}" > /dev/null

  echo "$RECTAG uploaded!"

  cd "$OLDDIR"

#+end_src
** Guess random song
#+begin_src fish :tangle guesssong :shebang "#!/usr/bin/env nix-shell"
  #!nix-shell -i fish -p fish ffmpeg fd

  set total 0
  set correct 0

  set OLDDIR (pwd)

  cd /mnt/media/010.006_Music/Archive/

  set files (mktemp)
  if [ "$argv[1]" = "--liked" ]
      echo Using Liked
      sleep 1

      cut --characters 12- ../Playlists/Liked.m3u8 | sort -R > $files
  else
      fd --type file --extension flac --extension mp3 --extension opus | sort -R > $files
  end

  for file in (cat $files)
      ffplay -v 0 -nodisp -autoexit "$file" &

      set output (cat $files | sort -R | fzf)
      if [ $file = $output ]
          echo "Correct ($file)"
          set correct (math $correct + 1)
      else
          echo "Wrong ($file)"
      end
      set total (math $total + 1)

      jobs -p | tail -1 | xargs kill

      read -l -P 'Do you want to continue? [Y/n] ' confirm
      switch $confirm
          case '' Y y
              continue
          case N n
              break
      end
  end

  rm $files
  echo "$correct/$total"

  cd "$OLDDIR"
#+end_src
** Upload playlist as liked songs to Listenbrainz
#+begin_src bash :tangle uploadliked :shebang "#!/usr/bin/env nix-shell"
  #!nix-shell -i bash -p bash flac vorbis-tools curl toybox

  set -euo pipefail

  TOKEN=$(cat @listenbrainzTokenFile@)

  OLDDIR=$(pwd)
  cd "$(dirname $1)"

  i=1
  FILE_LEN=$(cat "$(basename $1)" | wc -l)

  cat "$(basename "$1")" | while read line; do
      case $line in
          ,*.flac)
              RECTAG=$(metaflac --show-tag=MUSICBRAINZ_TRACKID "$line" | cut -c 21-);;
          ,*.ogg)
              RECTAG=$(vorbiscomment -l "$line" | grep MUSICBRAINZ_TRACKID | cut -c 21-);;
      esac

      curl -f -s -H "Authorization: Token $TOKEN" -H "Content-Type: application/json" \
           https://api.listenbrainz.org/1/feedback/recording-feedback \
           --data "{\"recording_mbid\": \"$RECTAG\", \"score\": 1}" > /dev/null

      echo "$RECTAG uploaded! ($i/$FILE_LEN)"
      i=$((i + 1))
      sleep 0.1 # rate limit!
  done

  cd "$OLDDIR"
#+end_src
