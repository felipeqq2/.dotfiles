# WebDAV (CardDAV+CalDAV) support
#
# Fetches my calendar and contacts from PurelyMail (my email provider) with vdirsyncer.
{
  pkgs,
  lib,
  osConfig,
  ...
}:
lib.mkIf osConfig.my.email {
  services.vdirsyncer = {
    enable = true;
    frequency = "*:0"; # hourly
  };
  programs.vdirsyncer.enable = true;

  accounts = let
    basePath = "/srv/syncthing/001.003_Vault";
  in {
    calendar = {
      inherit basePath;

      accounts.pmailCal = {
        primary = true;

        local = {
          type = "singlefile";
          fileExt = null; # singlefile type doesn't need/support fileExt
          path = "${basePath}/010.016_Calendar.ics";
        };

        remote = {
          passwordCommand = ["${pkgs.coreutils}/bin/cat" osConfig.age.secrets.email-password.path];
          type = "caldav";
          url = "https://purelymail.com/webdav/10723/caldav/default";
          userName = "hey@felipeqq2.rocks";
        };

        vdirsyncer = {
          enable = true;
          conflictResolution = "local wins";
        };
      };
    };

    contact = {
      inherit basePath;

      accounts.pmailCard = {
        local = {
          type = "singlefile";
          fileExt = null;
          path = "${basePath}/010.017_Contacts.vcf";
        };

        remote = {
          passwordCommand = ["${pkgs.coreutils}/bin/cat" osConfig.age.secrets.email-password.path];
          type = "carddav";
          url = "https://purelymail.com/webdav/10723/carddav/default";
          userName = "hey@felipeqq2.rocks";
        };

        vdirsyncer = {
          enable = true;
          conflictResolution = "local wins";
        };
      };
    };
  };
}
