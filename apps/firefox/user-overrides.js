// arkenfox overrides
user_pref("browser.startup.page", 3);                      // 0102
user_pref("browser.startup.homepage", "about:home");       // 0103
user_pref("browser.newtabpage.enabled", true);             // 0104
user_pref("browser.safebrowsing.malware.enabled", false);  // 0401
user_pref("browser.safebrowsing.phishing.enabled", false); // 0401

user_pref("browser.shell.shortcutFavicons", true); // 1006

user_pref("security.ssl.require_safe_negotiation", false); // 1201

user_pref("privacy.clearOnShutdown.history", false);                        // 2811
user_pref("privacy.clearOnShutdown_v2.historyFormDataAndDownloads", false); // 2811
user_pref("privacy.clearOnShutdown.cookies", false);                        // 2815
user_pref("privacy.clearOnShutdown_v2.cookiesAndStorage", false);           // 2815

user_pref("privacy.window.maxInnerWidth", 1000);  // 4502
user_pref("privacy.window.maxInnerHeight", 1000); // 4502

user_pref("signon.rememberSignons", false);                 // 5003
user_pref("browser.sessionstore.resume_from_crash", false); // 5008
user_pref("keyword.enabled", false);                        // 5021

user_pref("extensions.pocket.enabled", false); // 9000

// custom
user_pref("intl.accept_languages", "en-GB, en, pt-BR, pt"); // set display language
user_pref("browser.urlbar.suggest.calculator", true);       // enable urlbar calculator
user_pref("browser.urlbar.unitConversion.enabled", true);   // enable urlbar unit conversor
user_pref("browser.tabs.firefox-view", false);              // disable firefox view
user_pref("extensions.update.enabled", false);              // my add-ons are managed by nix
user_pref("extensions.autoDisableScopes", true);            // auto-enable nix add-ons
