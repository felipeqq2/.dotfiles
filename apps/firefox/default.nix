{
  pkgs,
  lib,
  osConfig,
  ...
}: let
  firefox = pkgs.firefox-beta;
in
  lib.mkIf osConfig.my.gui.enable {
    programs.firefox = {
      enable = true;
      package = firefox;

      profiles = let
        arkenfox = pkgs.fetchFromGitHub {
          owner = "arkenfox";
          repo = "user.js";
          rev = "133.0";
          hash = "sha256-iHj+4UGeB1FVGvOWB9ZZA4aiiJynBxRSFFfJqToYEdQ=";
        };
      in {
        default = {
          name = "Felipe Silva";

          extraConfig =
            (builtins.readFile "${arkenfox}/user.js")
            + (builtins.readFile ./user-overrides.js);

          extensions = with pkgs.nur.repos.rycee.firefox-addons; [
            ublock-origin
            keepassxc-browser
            (lib.mkIf osConfig.services.kubo.enable ipfs-companion)
            violentmonkey
            web-archives
            multi-account-containers
          ];

          bookmarks = [
            {
              name = "Toolbar";
              toolbar = true;
              bookmarks = [
                {
                  name = "WhatsApp";
                  url = "https://web.whatsapp.com";
                }
                {
                  name = "Google Drive";
                  url = "https://drive.google.com";
                }
                {
                  name = "Trakt.tv";
                  url = "https://trakt.tv";
                }
              ];
            }
            {
              name = "EAC Log Lookup";
              url = "https://eac-log-lookup.blogspot.com";
            }
            {
              name = "MusicBrainz";
              url = "https://musicbrainz.org";
              keyword = "mb";
            }
            (lib.mkIf osConfig.my.sash.enable {
              name = "SASH!";
              url = "http://localhost:9000";
              keyword = "sash";
            })
          ];

          search = {
            default = "DuckDuckGo";
            force = true;
            engines = {
              "Nix Packages" = {
                urls = [
                  {
                    template = "https://search.nixos.org/packages";
                    params = [
                      {
                        name = "query";
                        value = "{searchTerms}";
                      }
                    ];
                  }
                ];
                icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
                definedAliases = ["np"];
              };
              "Nix Options" = {
                urls = [
                  {
                    template = "https://search.nixos.org/options";
                    params = [
                      {
                        name = "query";
                        value = "{searchTerms}";
                      }
                    ];
                  }
                ];
                icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake.svg";
                definedAliases = ["no"];
              };
              "Noogle" = {
                urls = [
                  {
                    template = "https://noogle.dev";
                    params = [
                      {
                        name = "term";
                        value = ''"{searchTerms}"'';
                      }
                    ];
                  }
                ];
                icon = "${pkgs.nixos-icons}/share/icons/hicolor/scalable/apps/nix-snowflake-white.svg";
                definedAliases = ["noogle"];
              };

              "DuckDuckGo".metaData.alias = "d";
              "Google".metaData.alias = "g";
              "Bing".metaData.alias = "b";
              "Wikipedia (en)".metaData.alias = "w";

              "Mojeek" = {
                urls = [{template = "https://mojeek.com/search?q={searchTerms}";}];
                iconUpdateURL = "https://mojeek.com/favicon.ico";
                updateInterval = 7 * 24 * 60 * 60 * 1000; # every week
                definedAliases = ["m"];
              };
              "SearXNG in Space" = {
                urls = [{template = "https://search.im-in.space/search?q={searchTerms}";}];
                iconUpdateURL = "https://search.im-in.space/favicon.ico";
                updateInterval = 7 * 24 * 60 * 60 * 1000; # every week
                definedAliases = ["s"];
              };

              "Amazon.com".metaData.hidden = true;
            };
          };
        };
      };
    };

    home = {
      packages = [
        (pkgs.writeShellScriptBin "x-www-browser" ''
          exec ${firefox}/bin/firefox-beta "$@"
        '')
      ];
    };

    my.env.BROWSER = "${firefox}/bin/firefox-beta";
  }
