{
  config,
  osConfig,
  ...
}: {
  xdg.configFile."hut/config".source =
    config.lib.file.mkOutOfStoreSymlink
    osConfig.age.secrets.hut.path;
}
