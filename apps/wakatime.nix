{
  pkgs,
  config,
  osConfig,
  ...
}: {
  home.packages = [pkgs.wakatime];
  my.env.WAKATIME_HOME = "${config.xdg.configHome}/wakatime";

  xdg.configFile."wakatime/.wakatime.cfg".source =
    config.lib.file.mkOutOfStoreSymlink osConfig.age.secrets.wakatime.path;
}
