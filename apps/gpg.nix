{
  pkgs,
  osConfig,
  config,
  ...
}: {
  services.gpg-agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry.tty;
    extraConfig = "allow-emacs-pinentry";

    defaultCacheTtl = 14400;
    maxCacheTtl = 21600;
    enableSshSupport = true;
  };

  # Home-manager sets SSH_AUTH_SOCK for home.sessionVariables only
  # this path is a hash of GNUPGHOME, so it can be hardcoded. God Bless Arch Wiki.
  systemd.user.sessionVariables.SSH_AUTH_SOCK = "\${XDG_RUNTIME_DIR}/gnupg/d.9bmpciioh3t74rnr7akuxer1/S.gpg-agent.ssh";

  programs.gpg = {
    enable = true;
    settings = {
      default-key = (import ../keys.nix).felipeqq2.pgp;
      keyserver = "hkp://keys.openpgp.org:80";
    };

    # disable internal smart card support
    # and use pcscd (see modules/yubikey.nix)
    scdaemonSettings.disable-ccid = true;
  };

  # follow xdg desktop rules
  programs.gpg.homedir = "${config.xdg.dataHome}/gnupg";
  systemd.user.sessionVariables.GNUPGHOME = "${config.xdg.dataHome}/gnupg";
}
