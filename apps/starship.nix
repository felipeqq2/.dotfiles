{
  osConfig,
  config,
  ...
}: {
  programs.starship = {
    enable = true;

    settings = {
      battery.disabled =
        if osConfig.my.battery
        then false
        else true;

      package.disabled = true;

      character = {
        success_symbol = "[❯](bold green) ";
        error_symbol = "[✗](bold red) ";
      };

      cmd_duration = {
        min_time = 5000;
        format = "[$duration]($style) to complete";
        show_notifications = true;
        min_time_to_notify = 60000;
      };

      directory = {
        read_only = " read-only";
        truncation_symbol = "...";
      };

      git_branch = {
        style = "green";
        symbol = "";
      };

      git_commit.tag_disabled = false;

      git_state.style = "bold red";

      nodejs = {
        symbol = "node";
        format = "using [$symbol ($version )]($style)";
        style = "yellow";
      };

      nix_shell = {
        format = "[\\(with nix\\)]($style) ";
        heuristic = true;
      };

      kotlin = {
        symbol = "Kotlin ";
        format = "[$symbol($version )]($style)";
      };

      rust.symbol = "Rust ";
    };
  };

  my.env = {
    STARSHIP_CONFIG = "${config.xdg.configHome}/starship.toml";
    STARSHIP_CACHE = "${config.xdg.cacheHome}/starship";
  };
}
