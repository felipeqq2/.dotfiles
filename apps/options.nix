# Home manager custom options
{
  lib,
  config,
  ...
}: {
  options.my = with lib; {
    env = mkOption {
      default = {};
      type = with types; lazyAttrsOf (oneOf [str path int float]);
      description = "Environment variables to be set globally (including in systemd).";
      example = {
        EDITOR = "emacs";
        GS_OPTIONS = "-sPAPERSIZE=a4";
      };
    };
  };

  config = {
    home.sessionVariables = config.my.env;
    systemd.user.sessionVariables = config.my.env;
  };
}
