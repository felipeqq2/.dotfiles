# Legendary: Epic Games Launcher, but cooler and open-source
{
  lib,
  pkgs,
  config,
  ...
}: {
  xdg.configFile."legendary/config.ini".text = lib.generators.toINI {} {
    Legendary = {
      install_dir = "~/145_Games/legendary";

      # program will try to append these to config file if not present
      disable_update_check = false;
      disable_update_notice = false;
    };
    default.wine_executable = "${pkgs.wine}/bin/wine";

    # Cities Skylines (bypass launcher)
    bcbc03d8812a44c18f41cf7d5f849265.override_exe = "Cities.exe";
  };
}
