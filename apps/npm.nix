{
  config,
  osConfig,
  ...
}: {
  xdg.configFile."npm/config".source =
    config.lib.file.mkOutOfStoreSymlink osConfig.age.secrets.npmrc.path;
  my.env = {
    NPM_CONFIG_USERCONFIG = "${config.xdg.configHome}/npm/config";
    NPM_CONFIG_CACHE = "${config.xdg.cacheHome}/npm";
  };
}
