{
  lib,
  osConfig,
  config,
  pkgs,
  ...
}: {
  programs.swaylock.settings = let
    primary = "#ffffff";
    red = "#ff0000";
    transparent = "#00000000";
    wallpaper =
      if osConfig.my.gui.blurWallpaper
      then
        (pkgs.runCommand "blur-wallpaper" {}
          "${pkgs.imagemagick}/bin/magick ${osConfig.my.gui.wallpaper} -blur 0x12 $out")
      else "${osConfig.my.gui.wallpaper}";
  in
    lib.mkIf osConfig.my.gui.enable {
      image = "${wallpaper}";

      ring-color = primary;
      ring-clear-color = primary;
      ring-ver-color = primary;
      key-hl-color = primary;
      ring-wrong-color = red;
      bs-hl-color = red;

      text-color = transparent;
      text-clear-color = transparent;
      text-caps-lock-color = transparent;
      text-ver-color = transparent;
      text-wrong-color = transparent;
      line-color = transparent;
      line-clear-color = transparent;
      line-caps-lock-color = transparent;
      line-ver-color = transparent;
      line-wrong-color = transparent;
      inside-color = transparent;
      inside-clear-color = transparent;
      inside-ver-color = transparent;
      inside-wrong-color = transparent;
      separator-color = transparent;

      hide-keyboard-layout = true;
    };
}
