{
  pkgs,
  lib,
  osConfig,
  ...
}:
lib.mkIf osConfig.my.gui.enable {
  wayland.windowManager.sway = {
    enable = true;
    config = let
      mod = "Mod4"; # windows key
      terminal = "${pkgs.alacritty}/bin/alacritty";
      menu = "${pkgs.wofi}/bin/wofi";
      cmd = {
        mute = "exec ${pkgs.alsa-utils}/bin/amixer -q set Master toggle";
        lowerVol = "exec ${pkgs.alsa-utils}/bin/amixer -q set Master 30- unmute";
        raiseVol = "exec ${pkgs.alsa-utils}/bin/amixer -q set Master 30+ unmute";
      };
    in {
      inherit terminal menu;
      modifier = mod;
      input = {
        "type:keyboard" = with osConfig.services.xserver.xkb; {
          # inherit options from xserver config
          xkb_layout = layout;
          xkb_variant = variant;
          xkb_model = model;
          xkb_options = options;
          xkb_numlock = "enabled"; # enable numlock at startup
        };
        "type:touchpad" = {
          tap = "enabled";
          natural_scroll = "enabled";
        };
      };
      seat."*" = {
        hide_cursor = "5000";
        xcursor_theme = "phinger-cursors"; # see apps/theme/
      };
      gaps = {
        inner = 10;
        outer = -10; # remove gaps on edges
        smartGaps = true;
      };
      window.border = 0;
      bars = [];
      assigns = {
        # use `swaymsg -t get_tree` to fetch class and/or app_id
        "1" = [{app_id = "firefox";}];
        "3" = [{class = "Emacs";}];
        "4" = [{app_id = "Alacritty";}];
      };
      fonts = {
        names = ["Jetbrains Mono"];
        size = 11.0;
      };
      output."*".bg = "${osConfig.my.gui.wallpaper} fill";

      startup = [
        {command = "${pkgs.swaylock}/bin/swaylock";}
        {command = "${pkgs.brightnessctl}/bin/brightnessctl s 50%";}
      ];

      modes."(r)eboot, (p)oweroff" = {
        r = "exec reboot, mode default";
        p = "exec poweroff, mode default";
        Return = "mode default";
      };

      keybindings = {
        "${mod}+u" = "exec ${pkgs.swaylock}/bin/swaylock";
        "${mod}+Return" = "exec ${terminal}";
        "${mod}+Shift+q" = "kill";
        "${mod}+d" = "exec ${menu}";

        "${mod}+h" = "focus left";
        "${mod}+j" = "focus down";
        "${mod}+k" = "focus up";
        "${mod}+l" = "focus right";

        "${mod}+Left" = "focus left";
        "${mod}+Down" = "focus down";
        "${mod}+Up" = "focus up";
        "${mod}+Right" = "focus right";

        "${mod}+Shift+h" = "move left";
        "${mod}+Shift+j" = "move down";
        "${mod}+Shift+k" = "move up";
        "${mod}+Shift+l" = "move right";

        "${mod}+f" = "fullscreen toggle";
        "${mod}+Shift+f" = "floating toggle";
        "${mod}+s" = "mode resize";

        "${mod}+Shift+Left" = "move left";
        "${mod}+Shift+Down" = "move down";
        "${mod}+Shift+Up" = "move up";
        "${mod}+Shift+Right" = "move right";

        "${mod}+1" = "workspace number 1";
        "${mod}+2" = "workspace number 2";
        "${mod}+3" = "workspace number 3";
        "${mod}+4" = "workspace number 4";
        "${mod}+5" = "workspace number 5";
        "${mod}+6" = "workspace number 6";
        "${mod}+7" = "workspace number 7";
        "${mod}+8" = "workspace number 8";
        "${mod}+9" = "workspace number 9";

        "${mod}+Shift+1" = "move container to workspace number 1";
        "${mod}+Shift+2" = "move container to workspace number 2";
        "${mod}+Shift+3" = "move container to workspace number 3";
        "${mod}+Shift+4" = "move container to workspace number 4";
        "${mod}+Shift+5" = "move container to workspace number 5";
        "${mod}+Shift+6" = "move container to workspace number 6";
        "${mod}+Shift+7" = "move container to workspace number 7";
        "${mod}+Shift+8" = "move container to workspace number 8";
        "${mod}+Shift+9" = "move container to workspace number 9";

        "${mod}+Shift+minus" = "move scratchpad";
        "${mod}+minus" = "scratchpad show";

        "${mod}+Shift+c" = "reload";
        "${mod}+Shift+e" = ''mode "(r)eboot, (p)oweroff"'';

        XF86AudioMute = cmd.mute;
        "${mod}+F1" = cmd.mute;
        XF86AudioLowerVolume = cmd.lowerVol;
        "${mod}+F2" = cmd.lowerVol;
        XF86AudioRaiseVolume = cmd.raiseVol;
        "${mod}+F3" = cmd.raiseVol;

        XF86MonBrightnessUp = "${pkgs.brightnessctl}/bin/brightnessctl s +10";
        XF86MonBrightnessDown = "${pkgs.brightnessctl}/bin/brightnessctl s 10-";

        "${mod}+F4" = "exec ${pkgs.alsa-utils}/bin/amixer -q set Capture toggle";
        "${mod}+F10" = "exec ${pkgs.playerctl}/bin/playerctl previous";
        "${mod}+F11" = "exec ${pkgs.playerctl}/bin/playerctl play-pause";
        "${mod}+F12" = "exec ${pkgs.playerctl}/bin/playerctl next";

        # print screen
        "${mod}+Shift+s" = ''exec ${pkgs.grim}/bin/grim -t png -g "$(${pkgs.slurp}/bin/slurp)" - | ${pkgs.wl-clipboard}/bin/wl-copy --type image/png'';
        Print = "exec ${pkgs.grim}/bin/grim -t png - | ${pkgs.wl-clipboard}/bin/wl-copy --type image/png";

        "${mod}+c" = "exec ${pkgs.hyprpicker}/bin/hyprpicker | ${pkgs.wl-clipboard}/bin/wl-copy";

        "${mod}+e" = "exec ${pkgs.wofi-emoji}/bin/wofi-emoji";
      };
    };

    extraConfig = ''
      for_window [class=".*"] inhibit_idle fullscreen
      for_window [app_id=".*"] inhibit_idle fullscreen
    '';
  };

  home.packages = with pkgs; [wl-clipboard wtype imv];
}
