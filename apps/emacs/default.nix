{
  pkgs,
  config,
  osConfig,
  ...
}: let
  emacsPkg =
    if osConfig.my.gui.enable
    then pkgs.emacs-unstable
    else pkgs.emacs-unstable-nox;

  emacs = pkgs.emacsWithPackagesFromUsePackage {
    package = emacsPkg;
    config = ./init.org;

    # Assume ensuring all use-package declarations
    # use-package-always-ensure is t
    alwaysEnsure = true;

    # Assume tangling all emacs-lisp code blocks
    alwaysTangle = true;

    extraEmacsPackages = epkgs: with epkgs; [treesit-grammars.with-all-grammars];
  };

  inherit (pkgs.emacsPackagesFor emacsPkg) trivialBuild;

  # see https://github.com/terlar/emacs-config/blob/c622089/default.nix
  initPackage = trivialBuild {
    pname = "my-init";
    version = "";

    # init.org substitutions
    base16Slug = config.colorScheme.slug;

    unpackPhase = ''
      install -Dm644 ${./init.org} init.org
      substituteAllInPlace init.org
    '';

    buildPhase = ''
      emacs --batch --quick --load org init.org -f org-babel-tangle
    '';

    postInstall = ''
      ${pkgs.xorg.lndir}/bin/lndir -silent $out/share/emacs/site-lisp $out
    '';
  };
in {
  services.emacs = {
    enable = true;
    package = emacs;
    client.enable = true;
    defaultEditor = true;
    startWithUserSession = true;
  };

  xdg.configFile = {
    emacs = {
      source = initPackage;
      recursive = true; # folder should be writeable by Emacs
    };

    # add custom nix-colors-based emacs theme (thanks https://github.com/tinted-theming/base16-emacs)
    "emacs/${config.colorScheme.slug}-theme.el".text = with config.colorScheme; ''
      ;; MIT License
      ;; Copyright (C) 2012 Chris Kempson (http://chriskempson.com)
      ;; Copyright (C) 2016 Kaleb Elwert
      ;; Copyright (C) 2022 [Tinted Theming](https://github.com/tinted-theming)

      (require 'base16-theme)

      (defvar base16-${slug}-theme-colors
        '(:base00 "#${palette.base00}"
          :base01 "#${palette.base01}"
          :base02 "#${palette.base02}"
          :base03 "#${palette.base03}"
          :base04 "#${palette.base04}"
          :base05 "#${palette.base05}"
          :base06 "#${palette.base06}"
          :base07 "#${palette.base07}"
          :base08 "#${palette.base08}"
          :base09 "#${palette.base09}"
          :base0A "#${palette.base0A}"
          :base0B "#${palette.base0B}"
          :base0C "#${palette.base0C}"
          :base0D "#${palette.base0D}"
          :base0E "#${palette.base0E}"
          :base0F "#${palette.base0F}")
        "All colors for Base16 ${name} are defined here.")

      ;; Define the theme
      (deftheme base16-${slug})

      ;; Add all the faces to the theme
      (base16-theme-define 'base16-${slug} base16-${slug}-theme-colors)

      ;; Mark the theme as provided
      (provide-theme 'base16-${slug})

      (provide 'base16-${slug}-theme)
    '';
  };

  home.packages = with pkgs; [
    # needed for org-mode LaTeX
    (with texlive;
      combine {
        inherit scheme-basic latexmk;
        inherit wrapfig amsmath ulem hyperref capt-of xpatch;
        inherit dvipng; # image preview
        inherit bibtex biber biblatex; # citations
        inherit abntex2 biblatex-abnt; # abnt
        inherit cm-super; # fonts
        inherit polynom; # polynomials
        inherit babel babel-portuges; # multi-lang support
      })

    hunspell
    hunspellDicts.en_GB-large
    hunspellDicts.pt_BR

    mu # email

    perl # used in some magit commands
  ];

  # download csl-styles
  xdg.dataFile."csl-styles".source = pkgs.fetchFromGitHub {
    owner = "citation-style-language";
    repo = "styles";
    rev = "4b4e8f442d2de454c638393fbb9dd911c8b7aca7";
    hash = "sha256-oYRl4xg08U6jFOflxKVVenKHyVrtz0EIbjrA+9dj4Bo=";
  };
}
