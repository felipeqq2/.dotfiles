{
  pkgs,
  lib,
  osConfig,
  config,
  ...
}:
lib.mkIf osConfig.my.email {
  accounts.email = {
    maildirBasePath = "${config.xdg.dataHome}/email";
    accounts = let
      name = "Felipe Silva";
      primaryAddress = "hey@felipeqq2.rocks";

      # These credentials are linked to a personal testing app. Any emails should be added via the
      # console to enable login. See https://console.cloud.google.com/apis/credentials/oauthclient.
      # See https://github.com/MarcvdSluys/SundryNotes/blob/master/mbsync-with-gmail-oauth2.org
      oauth-command =
        "${pkgs.oauth2l}/bin/oauth2l fetch "
        + "--cache ${config.xdg.cacheHome}/oauth2l "
        + "--credentials ${osConfig.age.secrets.google-oauth.path} "
        + "--scope https://mail.google.com/ "
        + "--refresh"; # refresh expired token instead of reauthorizing

      acc = attr:
        lib.recursiveUpdate {
          mbsync = {
            enable = true;
            create = "both";
            remove = "both";
            expunge = "maildir";
          };
          mu.enable = true;
          msmtp.enable = true;
        }
        attr;
    in {
      primary = acc {
        primary = true;
        address = primaryAddress;
        userName = primaryAddress;
        realName = name;
        passwordCommand = "${pkgs.coreutils}/bin/cat ${osConfig.age.secrets.email-password.path}";
        folders.inbox = "INBOX";
        imap.host = "imap.purelymail.com";
        smtp.host = "smtp.purelymail.com";
        signature = {
          text = name;
          showSignature = "append";
        };
        gpg.key = primaryAddress;
      };
      unicamp = acc {
        flavor = "gmail.com";
        address = "f204706@dac.unicamp.br";
        realName = name;
        passwordCommand = oauth-command;
        msmtp.extraConfig.auth = "oauthbearer";
        mbsync = {
          extraConfig.account.AuthMechs = "XOAUTH2";
          patterns = [
            "*"
            "![Gmail]*"
            "[Gmail]/E-mails enviados"
            "[Gmail]/Lixeira"
            "[Gmail]/Rascunhos"
          ];
        };
        folders = {
          drafts = "[Gmail]/Rascunhos";
          sent = "[Gmail]/E-mails enviados";
          trash = "[Gmail]/Lixeira";
        };
        signature = {
          text = ''
            ${name} | RA 204706
            Engenharia de Computação (34)
          '';
          showSignature = "append";
        };
      };
      conpec = acc {
        flavor = "gmail.com";
        address = "felipe.silva@conpec.com.br";
        realName = name;
        passwordCommand = "${pkgs.coreutils}/bin/cat ${osConfig.age.secrets.conpec-email-password.path}";
        signature = {
          text = name;
          showSignature = "append";
        };

        mbsync.extraConfig.account.AuthMechs = "PLAIN"; # do not use oauth

        # fix gmail folder bullshit
        mbsync.patterns = [
          "*"
          "![Gmail]*"
          "[Gmail]/E-mails enviados"
          "[Gmail]/Lixeira"
          "[Gmail]/Rascunhos"
        ];
        folders = {
          drafts = "[Gmail]/Rascunhos";
          sent = "[Gmail]/E-mails enviados";
          trash = "[Gmail]/Lixeira";
        };
      };
    };
  };

  # add text signatures to ~/maildir/account/.signature
  home.file = let
    cfg = config.accounts.email;
    accounts =
      builtins.filter (acc: acc.signature.text != null)
      (lib.attrValues cfg.accounts);
  in
    builtins.listToAttrs (map (acc: {
        name = "${cfg.maildirBasePath}/${acc.name}/.signature";
        value.text = acc.signature.text;
      })
      accounts);

  programs = {
    mbsync.enable = true;
    mu.enable = true;
    msmtp.enable = true;
  };
}
