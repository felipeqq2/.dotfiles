{config, ...}: {
  home.shellAliases.mvn = "mvn -gs ${config.xdg.configHome}/maven/settings.xml";

  xdg.configFile."maven/settings.xml".text = ''
    <settings>
      <localRepository>''${env.XDG_DATA_HOME}/maven</localRepository>
    </settings>
  '';
}
