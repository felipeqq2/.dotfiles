{
  lib,
  osConfig,
  pkgs,
  config,
  ...
}:
lib.mkIf osConfig.my.gui.enable {
  home = {
    pointerCursor = {
      package = pkgs.phinger-cursors;
      name = "phinger-cursors";
      size = 24; # sway doesn't allow anything else (I like 24 anyway)
      gtk.enable = true;
    };

    # `home.pointerCursor` creates ~/.icons/ directory for backwards compatibility. I don't want it.
    file = {
      ".icons/default/index.theme".enable = false;
      ".icons/phinger-cursors".enable = false;
    };
  };

  gtk = let
    css =
      builtins.readFile (pkgs.substituteAll
        ({src = ./gtk.css;} // config.colorScheme.palette));
  in {
    enable = true;
    iconTheme = {
      package = pkgs.papirus-icon-theme;
      name = "Papirus";
    };

    gtk2.configLocation = "${config.xdg.configHome}/gtk-2.0/gtkrc"; # unclutter $HOME

    theme.package = pkgs.adw-gtk3;
    theme.name = "adw-gtk3";

    gtk3.extraCss = css;
    gtk4.extraCss = css;

    gtk3.extraConfig.gtk-application-prefer-dark-theme = true;
    gtk4.extraConfig.gtk-application-prefer-dark-theme = true;
  };
  dconf.settings."org/gnome/desktop/interface".color-scheme = "prefer-dark";

  qt = {
    enable = true;
    platformTheme.name = "gtk3";
  };
}
