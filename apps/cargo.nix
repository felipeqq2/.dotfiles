{
  lib,
  config,
  osConfig,
  ...
}: {
  xdg.dataFile."cargo/credentials.toml".source =
    config.lib.file.mkOutOfStoreSymlink
    osConfig.age.secrets.rust-credentials.path;

  my.env.CARGO_HOME = "${config.xdg.dataHome}/cargo";
}
