{
  pkgs,
  lib,
  config,
  ...
}: {
  programs.fish = {
    enable = true;
    shellAliases = {
      cat = "${pkgs.bat}/bin/bat";
    };
    loginShellInit = lib.mkIf config.wayland.windowManager.sway.enable ''
      if test ! $DISPLAY && test (tty) = "/dev/tty1"
         exec sway
      end
    '';
  };
}
