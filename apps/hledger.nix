{
  pkgs,
  config,
  ...
}: {
  home.packages = [pkgs.hledger];
  my.env.LEDGER_FILE = "${config.home.homeDirectory}/010_Resources/010.009_Finances.journal";
}
