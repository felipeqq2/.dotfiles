{
  lib,
  pkgs,
  osConfig,
  ...
}: {
  services.swayidle = lib.mkIf osConfig.my.gui.enable {
    enable = true;
    timeouts = [
      {
        timeout = 50;
        command = "${pkgs.brightnessctl}/bin/brightnessctl -s s 0";
        resumeCommand = "${pkgs.brightnessctl}/bin/brightnessctl -r";
      }
      {
        timeout = 60;
        command = "${pkgs.swaylock}/bin/swaylock -f";
      }
    ];

    events = [
      {
        event = "before-sleep";
        command = "${pkgs.swaylock}/bin/swaylock -f";
      }
    ];
  };
}
