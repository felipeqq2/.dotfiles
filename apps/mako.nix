{
  osConfig,
  pkgs,
  lib,
  ...
}:
lib.mkIf osConfig.my.gui.enable {
  services.mako = {
    enable = true;
    font = "sans 12";
    backgroundColor = "#09111f";
    borderColor = "#00000000";
    layer = "overlay";
    defaultTimeout = 5000;
    padding = "15,20";
  };
}
