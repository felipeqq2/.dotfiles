{
  pkgs,
  osConfig,
  ...
}: {
  programs.waybar = {
    enable = true;
    systemd.enable = true;
    style = ./waybar.css;
    settings = let
      network = {
        format-ethernet = "Connected: ";
        format-wifi = "{essid}: ";
        format-linked = "";
        format-disconnected = "";
        tooltip-format = "{ifname} is {ipaddr}";
        tooltip-format-disconnected = "Disconnected";
      };
    in [
      {
        position = "top";
        fixed-center = false;
        modules-left = ["sway/workspaces" "sway/mode"];
        modules-right = [
          "tray"
          "pulseaudio"
          "network#enp*"
          "network#wlp*"
          "backlight"
          "idle_inhibitor"
          (
            if osConfig.my.battery
            then "battery"
            else ""
          )
          "clock"
        ];

        "sway/window".tooltip = false;
        "sway/mode".tooltip = false;
        clock.format = "{:%Y-%m-%d %a ~ <b>%H:%M</b>}";

        idle_inhibitor = {
          format = "caffeine: {icon}";
          format-icons = {
            activated = "on";
            deactivated = "off";
          };
        };

        battery = {
          states = {
            warning = "30";
            critical = "15";
          };
          format-icons = ["" "" "" "" ""];
          format-charging = "{capacity}% ";
          format = "{capacity}% {icon}";
        };

        backlight = {
          format-icons = ["" "" ""];
          format = "{percent}% {icon}";
          on-scroll-up = "${pkgs.brightnessctl}/bin/brightnessctl s +10";
          on-scroll-down = "${pkgs.brightnessctl}/bin/brightnessctl s 10-";
        };

        "network#enp*" = network // {interface = "enp*";};
        "network#wlp*" = network // {interface = "wlp*";};

        pulseaudio = {
          format = "{volume}% {icon}";
          format-muted = "Muted";
          format-icons = ["" "" ""];
          on-click = "${pkgs.alsa-utils}/bin/amixer -q set Master toggle";
        };
      }
    ];
  };
}
