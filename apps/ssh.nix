_: {
  programs.ssh = {
    enable = true;
    extraOptionOverrides = {
      VisualHostKey = "yes";
    };

    # refresh TTY every time ssh is run (see https://unix.stackexchange.com/a/499133)
    extraConfig = ''
      Match host * exec "gpg-connect-agent UPDATESTARTUPTTY /bye"
    '';
  };
}
