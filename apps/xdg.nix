# Keep everything in their proper XDG directories.
# Specific configuration is in each app's respective configuration file (apps/{app}).
#
# Programs still littering $HOME:
# - Mozilla Firefox (~/.mozilla/) [see https://bugzil.la/259356]
# - OpenSSH (~/.ssh/) [see http://archive.today/SQ1WD - BUG 2050]
# - mbsync (~/.mbsyncrc) [see github:nix-community/home-manager#3317]
#
# (see more at https://wiki.archlinux.org/title/XDG_Base_Directory)
{
  config,
  osConfig,
  ...
}: let
  home = config.home.homeDirectory;
  my = osConfig.my.xdg;

  cacheHome = "${home}/${my.cache}";
  configHome = "${home}/${my.config}";
  dataHome = "${home}/${my.data}";
  stateHome = "${home}/${my.state}";
in {
  xdg = {
    enable = true;
    inherit cacheHome configHome dataHome stateHome;

    # Manage $XDG_CONFIG_HOME/user-dirs.dirs
    userDirs = {
      enable = true;
      download = "${home}";
      desktop = "${home}";
    };
  };

  my.env = {
    # patches for dumb programs not using XDG
    TBB_HOME = "${dataHome}/tor-browser";
    GOPATH = "${dataHome}/go";
    GOMODCACHE = "${cacheHome}/go";
    WINEPREFIX = "${dataHome}/wine";
    _JAVA_OPTIONS = "-Djava.util.prefs.userRoot=${configHome}/java";
    GRADLE_USER_HOME = "${dataHome}/gradle";
  };
}
