{
  inputs,
  osConfig,
  pkgs,
  ...
}: {
  news.display = "silent"; # notifications about home-manager
  manual.html.enable = true;

  home = {
    inherit (osConfig.my) username;
    homeDirectory = "/home/${osConfig.my.username}";
    stateVersion = "22.05";

    packages = with pkgs; [
      bat # cat with wings
      ripgrep # a better grep
      fd # a better find
      fzf # fuzzy finder
      sd # a better sed
      tealdeer # better, faster man pages, now even faster
      rage # file encryption
      xdg-utils # xdg-open and friends
      moreutils # more utils
      pls # pls!
      rm-improved # rm safely
      keepassxc # omg password manager hiii
    ];
  };

  my.env = {
    # don't track me!
    GOPROXY = "direct";
    # don't fetch random binaries!
    GOTOOLCHAIN = "local";
  };

  programs = {
    home-manager.enable = true;
    zoxide.enable = true;
    man.generateCaches = false;
  };

  # import all files in `./apps` directory
  imports = with builtins;
    map (file: ./apps + "/${file}") (attrNames (readDir ./apps))
    # Extra Home-Manager modules
    #
    # I'd rather import extra home-manager modules on the appropriate files in the `/apps/` folder,
    # but apparently I can't import a module inside an imported module? It throws "The option
    # `home-manager.users.my-user.imports' does not exist". I'm doing something wrong.
    ++ [inputs.nix-colors.homeManagerModules.default];
}
